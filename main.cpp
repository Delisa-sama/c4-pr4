/*
 * Нахождение минимума всех элементов, больших нуля;
 * удаление всех отрицательных элементов
 * и добавление после каждого элемента списка числа, на единицу большего.
 */

#include <iostream>
#include <stdexcept>
#include <string.h>
#include <vector>

int main( int argc, char **argv ) {
	std::vector<int> vi;
	std::vector<int> vo;
	std::string		 line;

	// INPUT
	std::cout << "Type the numbers in int type range. Type any non-number "
				 "string to stop."
			  << std::endl;
	while ( 1 ) {
		std::cin >> line;
		int el;
		try {
			el = std::stoi( line, nullptr );
		} catch ( std::exception &e ) {
			std::cout << "end of input" << std::endl;
			break;
		}
		vi.push_back( el );
	};

	// PROCESSING
	int min = vi.front();
	for ( auto it = vi.begin(); it != vi.end(); ++it ) {
		if ( *it > 0 && ( *it < min || min <= 0 ) )
			min = *it;

		if ( *it >= 0 ) {
			vo.push_back( *it );
			vo.push_back( *it + 1 );
		}
	}

	// OUTPUT
	std::cout << "Results" << std::endl << "min: " << min << std::endl;
	for ( auto it = vo.begin(); it != vo.end(); ++it ) {
		std::cout << *it << std::endl;
	}

	return 0;
}
