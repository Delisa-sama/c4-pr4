cmake_minimum_required(VERSION 3.10)
add_compile_options(-std=c++14)

project(pr4)

SET(SRCS
	main.cpp
)

add_executable(pr4 "${SRCS}" )
